

ENV = "development"
DEBUG = ENV == "development"
API_KEY = "secret"

JSON_SORT_KEYS = False

# Pagination
ITEMS_PER_PAGE = 10

QUERY_PER_PAGE = "per_page"
QUERY_SORT = "sort"
QUERY_PAGE = "page"

SKIP_FILTERS = (QUERY_PER_PAGE, QUERY_SORT, QUERY_PAGE,)

VIEW_MODULE = "views"
ENDPOINTS = {
    "cases": {
        "auth": True
    }
}

MONGO_URI = "mongodb://localhost:27017/aivo"
