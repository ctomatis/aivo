from importlib import import_module

from cerberus import Validator
from aivo.utils import config


def validate(payload, endpoint):
    """
    This is a lazy validation...
    A robust validation should be against the database.

    :param payload: The document being validated.
    :param endpoint: The requested endpoint.

    :return Validator
    """

    # Load the validation schema associated to the requested endpoint.
    schema = load_schema(endpoint)

    # Load validator and perform payload validation
    v = Validator()
    v.require_all = True
    v.validate(payload, schema)

    return v


def validate_filters(payload, endpoint):
    schema = load_schema(endpoint, "filter_schema")

    payload = {k: v.lstrip("!><[]=")
               for k, v in payload.items() if k not in config.SKIP_FILTERS}

    v = Validator()
    v.validate(payload, schema)

    return v


def load_schema(name, schema_type=None):
    schema_module = import_module(".schemas.{}".format(name), __package__)
    return getattr(schema_module, schema_type or "schema")
