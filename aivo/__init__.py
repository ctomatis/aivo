from .flaskapp import Aivo
from aivo.services import mongo


def create_app(settings):

    app = Aivo(settings=settings)
    mongo.init_app(app)

    return app
