from importlib import import_module

from flask import Flask, jsonify
from werkzeug.exceptions import HTTPException, default_exceptions

from aivo.io import ApiJSONEncoder, auth, render
from aivo.views import Schemas


class Aivo(Flask):

    def __init__(self, import_name=__package__, settings=None, **options):
        super(Aivo, self).__init__(import_name, **options)

        # Load config from pyfile
        self.config.from_pyfile(settings)

        # Custom json serializar
        self.json_encoder = ApiJSONEncoder

        # Register endpoints
        self.register_views()

        # Load error handler
        self.http_error_handlers()

    def run(self, host=None, port=None, debug=None, **options):
        """
        Run the app.
        """

        super(Aivo, self).run(host, port, debug, **options)

    def register_views(self):
        """
        This factory register the endpoints.
        """
        # Register api resources
        for endpoint, options in self.ENDPOINTS.items():
            self.register_url(endpoint, **options)

        # Register schema endpoint
        self.add_url_rule(
            "/schemas/<name>",
            view_func=Schemas.as_view("schemas"),
            methods=["GET"])

    def register_url(self, endpoint, **options):
        """
        Register a single url/endpoint to the ´view class´ in module ´views´.
        """

        module = import_module(".%s" % self.VIEW_MODULE, __package__)

        klass = getattr(module, endpoint.capitalize())

        meths = [v for v in ("get", "post", "put", "delete",)
                 if getattr(klass, v, None)]

        view_fn = klass.as_view(endpoint)

        if options.get("auth", True):
            view_fn = auth(view_fn)

        view_fn = render(view_fn)

        self.add_url_rule("/{}".format(endpoint),
                          view_func=view_fn, methods=meths)

    def __getattr__(self, name):
        return self.config.get(name, None)

    def std_handler(self, error):
        code = 500
        msg = "Application error"

        if isinstance(error, HTTPException):
            code = error.code
            msg = error.description

        response = jsonify(error=msg, status=code)
        response.status_code = code
        return response

    def http_error_handlers(self):
        self.register(HTTPException)
        for code, v in default_exceptions.items():
            self.register(code)

    def register(self, exception_or_code, handler=None):
        self.errorhandler(exception_or_code)(handler or self.std_handler)
