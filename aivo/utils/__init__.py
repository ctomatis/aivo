from flask import current_app as app


class Config(object):
    """
    Config helper
    """

    def __getattr__(self, name):
        return app.config.get(name, None)


config = Config()
