

def build_filter_schema(schema):
    rv = {}
    for k, v in schema.items():
        if "schema" in v:
            for i, s in v["schema"].items():
                filt = "{}.{}".format(k, i)
                rv[filt] = {
                    'type': s.get("type"),
                    'required': False,
                    'nullable': s.get("nullable", False),
                    'empty': False
                }
        else:
            v.update(required=False)
            rv[k] = v
    return rv
