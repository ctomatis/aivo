from functools import wraps

from aivo.utils import config
from aivo.utils.parser import parse
from aivo.validation import validate, validate_filters
from flask import abort
from flask import current_app as app
from flask import jsonify, request


def render(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        # The header "content-type" (application/json)
        # is checked in ´prodution´ mode.

        if not request.is_json and not app.debug:
            abort(415)

        req, status, endpoint = None, 200, request.endpoint

        try:
            if "GET" in request.method:
                # Validate filters
                vf = validate_filters(dict(request.args), endpoint)

                if vf.errors:
                    status = 400
                    return jsonify(dict(errors=vf.errors, status=status)), status

                req = parse(request.query_string.decode())

                print("Mongo Query: %s" % req)

            else:
                req = request.get_json()
        except:
            abort(404)

        if "POST" in request.method:
            # Validate payload
            v = validate(req, endpoint)
            if v.errors:
                status = 422
                return jsonify(dict(errors=v.errors, status=status)), status
            req, status = v.document, 201

        rv = f(*args, **req)

        if not rv:
            abort(404)

        return jsonify(rv), status

    return decorated
