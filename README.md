# Aivo


### Installation

Aivo requires a [MongoDB](https://docs.mongodb.com/manual/administration/install-community/) instance and Python 3.6+ to run.

Get the git checkout of ***Aivo*** package in a new virtualenv and run in development mode.

```sh
$ git clone https://ctomatis@bitbucket.org/ctomatis/aivo.git
$ cd aivo
$ virtualenv .env # or python3 -m venv .env
$ . .env/bin/activate
(.env)$ pip install .
```

### Configuration
This readme assumes that a MongoDB instance is running on the default host and port `localhost:27017`. You can also set explicitly `MONGO_URI`, which is in the file `settings.py`.

```sh
# Populate database
(.env)$ python mock.py
# -- Reading data file '/aivo/data.csv' --
# -- Connecting to MongoDB --
# -- Use database ´aivo´ --
# -- Create index for ´value´ and ´country´ fields --
# -- Inserted ´3324´ documents in collection ´cases´ --
```

### Run
```sh
(.env)$ python app.py
# * Serving Flask app "aivo" (lazy loading)
# * Environment: development
# * Debug mode: on
# * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
# * Restarting with stat
# * Debugger is active!
# ...
```

### Examples

```sh
# Install dev dependecies
(.env)$ pip install -r requirements-dev.txt

# Filter cases
(.env)$ python aivo/examples/get_cases.py

# Create a new case
(.env)$ python aivo/examples/post_case.py
```

### Filter operators

| Operation | QS |
| ------ | ------ |
| equal | ?field_path=value |
| unequal | ?field_path=!value |
| greater than | ?field_path=>value |
| less than | ?field_path=<value |
| greater than or equal to | ?field_path=>=value |
| less than or equal to | ?field_path=<=value |



### Sort operators

| Prefix | QS | Order type |
| ------ | ------ | ------ |
| - | ?sort=-field_path | desc
| (empty) | ?sort=field_path | asc

***Sort operators*** can be combined, see examples.


### Pagination

| param | QS | Description |
| ------ | ------ | ------ |
| per_page | ?per_page=20 | items per page (defaults to 10)
| page | ?page=1 | current page


### For instance...

Find cases where `value >= 2.5` and `inequality.code = TOT`. Sort results by `value DESC` and `country.name ASC` 


```sh
curl -L -X GET 'http://localhost:5000/cases?value=>=2.5&inequality.code=TOT&sort=-value,country.name' \
-H 'Content-Type: application/json' \
-H 'Authorization: secret'

# ...
# {
#  "_items": [...], 
#  "_total": 757, 
#  "_pagination": {
#    "per_page": 10, 
#    "current_page": 1, 
#    "pages": 76, 
#    "links": {
#      "self": "http://localhost:5000/cases?value=%3E%3D2.5&inequality.code=TOT&sort=-value%2Ccountry.name&page=1", 
#      "last": "http://localhost:5000/cases?value=%3E%3D2.5&inequality.code=TOT&sort=-value%2Ccountry.name&page=76", 
#      "next": "http://localhost:5000/cases?value=%3E%3D2.5&inequality.code=TOT&sort=-value%2Ccountry.name&page=2", 
#      "prev": "http://localhost:5000/cases?value=%3E%3D2.5&inequality.code=TOT&#sort=-value%2Ccountry.name&page=1"
#    }
#  }
# }
```

Create a new case
```sh
curl -L -X POST 'http://localhost:5000/cases' \
-H 'Content-Type: application/json' \
-H 'Authorization: secret' \
--data-raw '{
    "country" : {
        "code" : "IRL",
        "name" : "Ireland"
    },
    "indicator" : {
        "code" : "JE_LMIS",
        "name" : "Labour market insecurity"
    },
    "measure" : {
        "code" : "L",
        "name" : "Value"
    },
    "unit" : {
        "code" : "PC",
        "name" : "Percentage"
    },
    "powercode" : {
        "code" : "0",
        "name" : "Units"
    },
    "period" : {
        "code" : null,
        "name" : null
    },
    "flag" : {
        "code" : null,
        "name" : null
    },
    "inequality" : {
        "code" : "TOT",
        "name" : "Total"
    },
    "value" : 2.5
}'

# {
#    "country": {
#        "code": "IRL",
#        "name": "Ireland"
# ...
#    "value": 2.5,
#    "_created": "2020-10-27T17:48:38",
#    "_id": "5f985d76f51acb5368932cfa"
# }
```