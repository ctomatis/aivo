from aivo.validation import load_schema
from flask import abort, jsonify
from flask.views import MethodView


class Schemas(MethodView):

    def get(self, name=None):
        try:
            return jsonify(load_schema(name))
        except:
            abort(404)
