from requests import get
from pprint import pprint

url = "http://localhost:5000/cases"


def run():
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'secret'
    }

    params = {
        "value": ">=2.5",
        "inequality.code": "TOT",
        "sort": "-value,country.name"
    }

    res = get(url, headers=headers, params=params)

    pprint(res.json())


if __name__ == "__main__":
    run()
