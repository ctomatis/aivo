from requests import post
from pprint import pprint

url = "http://localhost:5000/cases"

def run():
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'secret'
    }

    payload = {
        "country": {
            "code": "IRL",
            "name": "Ireland"
        },
        "indicator": {
            "code": "JE_LMIS",
            "name": "Labour market insecurity"
        },
        "measure": {
            "code": "L",
            "name": "Value"
        },
        "unit": {
            "code": "PC",
            "name": "Percentage"
        },
        "powercode": {
            "code": "0",
            "name": "Units"
        },
        "period": {
            "code": None,
            "name": None
        },
        "flag": {
            "code": None,
            "name": None
        },
        "inequality": {
            "code": "TOT",
            "name": "Total"
        },
        "value": 2.5
    }

    res = post(url, headers=headers, json=payload)

    pprint(res.json())


if __name__ == "__main__":
    run()
