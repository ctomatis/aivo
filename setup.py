import io
import os

from setuptools import find_packages, setup


def read(*parts):
    __DIR__ = os.path.abspath(os.path.dirname(__file__))
    return io.open(os.path.join(__DIR__, *parts), 'r')


requires = [_.strip() for _ in read('requirements.txt') if _.strip()]

setup(
    name="aivo",
    version="0.0.1",
    description="",
    packages=find_packages(),
    include_package_data=True,
    author="Cristian Tomatis",
    author_email="cgtomatis@gmail.com",
    url="https://ctomatis@bitbucket.org/ctomatis/aivo.git",
    platforms=['any'],
    install_requires=requires,
    python_requires=">=3.6",
    classifiers=[
        'Development Status :: Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
