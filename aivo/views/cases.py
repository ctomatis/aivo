from aivo.io.db import driver
from flask import abort
from flask.views import MethodView


class Cases(MethodView):

    def get(self, **kwargs):
        return driver.find(kwargs)

    def post(self, **kwargs):
        return driver.save(kwargs)
