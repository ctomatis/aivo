from datetime import datetime

from bson.objectid import ObjectId
from flask.json import JSONEncoder


class ApiJSONEncoder(JSONEncoder):
    def default(self, obj):
        
        if isinstance(obj, datetime):
            return obj.replace(microsecond=0).isoformat()

        if isinstance(obj, ObjectId):
            return str(obj)

        if isinstance(obj, type):
            return obj.__name__

        return JSONEncoder.default(self, obj)
