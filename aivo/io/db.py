from datetime import datetime
from math import ceil
from urllib.parse import urlencode

from aivo.services import mongo
from aivo.utils import config
from flask import abort, request
from pymongo import ASCENDING, DESCENDING


class MongoDb(object):

    __db = None

    @property
    def collection(self):
        return self.cnx[request.endpoint]

    @property
    def cnx(self):
        if self.__db is None:
            self.__db = mongo.db
        return self.__db

    def find(self, query):
        """
        Retrieves a set of documents matching a given request. Query is
        expressed in format of the mongo query syntax.

        Returns 404 if resultset is empty, otherwise the 
        resultset is paginated.
        """

        count = self.collection.count_documents(query)

        if not count:
            abort(404, description="Not found. Query does not match any document.")

        params = dict(request.args)
        per_page = self.per_page
        page = self.current_page
        sort = self.sort

        skip = per_page * (page-1)
        pages = ceil(count/per_page)
        next_page = page + 1 if page < pages else pages
        prev_page = page - 1 if page > 1 else 1

        q = self.collection.find(query)
        if sort:
            q.sort(sort)

        rv = {
            "_items": list(q.skip(skip).limit(per_page)),
            "_total": count,
            "_pagination": {
                "per_page": per_page,
                "current_page": page,
                "pages": pages,
                "links": {
                    "self": self.get_link(params, page),
                    "last": self.get_link(params, pages),
                    "next": self.get_link(params, next_page),
                    "prev": self.get_link(params, prev_page)
                }
            },

        }

        return rv

    def save(self, document):
        """
        Create a new document and return it.
        """
        now = datetime.utcnow().replace(microsecond=0)
        document.update(_created=now)

        self.collection.insert_one(document)
        return document

    def get_link(self, params, page):
        params.update(page=page)
        return "{}?{}".format(request.base_url, urlencode(params))

    @property
    def current_page(self):
        page = 1
        try:
            page = abs(int(request.args.get("page", page)))
        except:
            pass
        return page

    @property
    def per_page(self):
        per_page = config.ITEMS_PER_PAGE
        try:
            per_page = abs(int(request.args.get("per_page", per_page)))
        except:
            pass
        return per_page

    @property
    def sort(self):
        try:
            sorting = request.args.get("sort", "").strip()
            if sorting:
                return [(_.replace("-", ""), DESCENDING if _[0] == "-" else ASCENDING,)
                        for _ in sorting.split(",")]
        except:
            pass


driver = MongoDb()
