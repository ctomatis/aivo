from aivo.validation.schemas import build_filter_schema

schema = {
    'powercode': {'schema': {'code': {'type': 'string'}, 'name': {'type': 'string'}}, 'type': 'dict'},
    'country': {'schema': {'code': {'type': 'string'}, 'name': {'type': 'string'}}, 'type': 'dict'},
    'unit': {'schema': {'code': {'type': 'string'}, 'name': {'type': 'string'}}, 'type': 'dict'},
    'indicator': {'schema': {'code': {'type': 'string'}, 'name': {'type': 'string'}}, 'type': 'dict'},
    'inequality': {'schema': {'code': {'type': 'string'}, 'name': {'type': 'string'}}, 'type': 'dict'},
    'measure': {'schema': {'code': {'type': 'string'}, 'name': {'type': 'string'}}, 'type': 'dict'},
    'flag': {'schema': {'code': {'nullable': True, 'type': 'string'}, 'name': {'nullable': True, 'type': 'string'}}, 'type': 'dict'},
    'period': {'schema': {'code': {'nullable': True, 'type': 'string'}, 'name': {'nullable': True, 'type': 'string'}}, 'type': 'dict'},
    'value': {'coerce': float, 'min': 0, 'type': 'float'}
}

filter_schema = build_filter_schema(schema)
