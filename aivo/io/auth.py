from functools import wraps

from aivo.utils import config
from flask import current_app as app
from flask import jsonify, request, abort
from flask.json import loads


def auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth_header = request.headers.get("Authorization", "").strip()

        if auth_header != config.API_KEY and not app.debug:
            abort(401)

        return f(*args, **kwargs)

    return decorated
