import io
import os
from datetime import datetime

import unicodecsv as csv
from pymongo import ASCENDING, MongoClient

import settings

__DIR__ = os.path.dirname(os.path.abspath(__file__))

NOW = datetime.utcnow().replace(microsecond=0)



def mocker():
    rows, filename = [], os.path.join(__DIR__, "data.csv")

    print("-- Reading data file '%s' --" % filename)
    with io.open(filename, "r", encoding="utf-8-sig") as fp:
        reader = csv.DictReader((l.encode('utf-8') for l in fp))
        for row in reader:
            if len(row["LOCATION"]) != 3:
                continue
            rows += [map_keys(row)]
        fp.close()

    if rows:
        print("-- Connecting to MongoDB --")
        try:
            client = MongoClient(settings.MONGO_URI)
            print("-- Use database ´aivo´ --")
            db = client.aivo
            db.cases.drop()

            print("-- Create index for ´value´ and ´country´ fields --")

            db.cases.create_index(
                [("value", ASCENDING), ("country.name", ASCENDING)])

            db.cases.insert_many(rows)

            print("-- Inserted ´%d´ documents in collection ´cases´ --" %
                  db.cases.count_documents({}))

            client.close()

        except Exception as e:
            print("-- Connection failed! --")
            print(e)


def map_keys(row):

    row = {k: None if not v.strip() else v.strip() for k, v in row.items()}

    rv = {
        "country": {"code": row["LOCATION"], "name": row["Country"]},
        "indicator": {"code": row["INDICATOR"], "name": row["Indicator"]},
        "measure": {"code": row["MEASURE"], "name": row["Measure"]},
        "unit": {"code": row["Unit Code"], "name": row["Unit"]},
        "powercode": {"code": row["PowerCode Code"], "name": row["PowerCode"]},
        "period": {"code": row["Reference Period Code"], "name": row["Reference Period"]},
        "flag": {"code": row["Flag Codes"], "name": row["Flags"]},
        "inequality": {"code": row["INEQUALITY"], "name": row["Inequality"]},
        "value": as_float(row["Value"]),
        "_created": NOW
    }

    return rv


def as_float(value):
    try:
        value = float(value)
    except:
        value = 0.0
    return value


if __name__ == "__main__":
    mocker()
