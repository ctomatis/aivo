import os
from aivo import create_app

_DIR_ = os.path.dirname(os.path.abspath(__file__))
cfgfile = os.path.join(_DIR_, "settings.py")

app = create_app(cfgfile)

if __name__ == "__main__":
    app.run()
