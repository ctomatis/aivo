from urllib import parse as ulib_parse
from aivo.utils import config


class QStoMongoQuery(object):
    def __init__(self, qs, exclude_params=None):
        self.qs = qs
        self.exclude_params = exclude_params or config.SKIP_FILTERS
        self.query = {}
        self.operators = ['!', '>', '<']

        self.operator_map = {
            '!': '$ne',
            '>': '$gt',
            '>=': '$gte',
            '<': '$lt',
            '<=': '$lte',
            '[]': '$in',
            '[!]': '$nin'
        }

    @staticmethod
    def qs_to_dict(qs):
        return ulib_parse.parse_qs(qs)

    def guess_operator(self, value):
        if value.startswith('<='):
            return '$lte', float(value[2:])

        if value.startswith('>='):
            return '$gte', float(value[2:])

        if value.startswith('<'):
            return '$lt', float(value[1:])

        if value.startswith('>'):
            return '$gt', float(value[1:])

        if value.startswith('!'):
            return '$ne', value[1:]

        if value.startswith('[]'):
            return '$in', value[2:]

        if value.startswith('[!]'):
            return '$nin', value[3:]

        return None, value

    def finalize_sub_query(self, key, sub_query):
        _sub_query = {}
        for _key, value in sub_query.items():

            if _key is None:
                return {key: self.to_bool(value[-1])}

            elif _key not in ('$in', '$nin'):
                value = self.to_bool(value[-1])

            _sub_query.update({_key: value})

        return {key: _sub_query}

    def to_bool(self, value):
        
        if not isinstance(value, str):
            return value

        bool_mapping = {
            'true': True,
            'false': False,
            'null': None
        }

        return bool_mapping.get(value.lower(), value)

    def parse(self):
        parsed_qs = {k: v for k, v in self.qs_to_dict(
            self.qs).items() if k not in self.exclude_params}

        for key, value in parsed_qs.items():
            sub_query = {}

            for element in value:
                operator, _element = self.guess_operator(element)

                if not sub_query.get(operator, None):
                    sub_query[operator] = [_element]
                else:
                    sub_query[operator].append(_element)
            self.query.update(self.finalize_sub_query(key, sub_query))

        return self.query


def parse(qs, exclude_params=None):
    parser = QStoMongoQuery(qs, exclude_params)
    return parser.parse()
